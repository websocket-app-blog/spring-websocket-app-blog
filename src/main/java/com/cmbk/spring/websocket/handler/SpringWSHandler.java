package com.cmbk.spring.websocket.handler;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.cmbk.spring.websocket.util.WebsocketUtil;

/**
 * @author chanaka.k
 *
 */
public class SpringWSHandler extends TextWebSocketHandler {

	private static Logger logger = LoggerFactory.getLogger(SpringWSHandler.class);
	
	@Value("${ws.url}")
	private static String wsUrl;
	
	private StringBuilder partialMessageResponseBuilder = null;

	public static void executeSpringWebSocket() {

		/*
		 * A WebSocket connection manager that is given a URI, a WebSocketClient, and a WebSocketHandler, connects to a WebSocket server through start() and stop() methods. 
		 * If setAutoStartup(boolean) is set to true this will be done automatically when the Spring ApplicationContext is refreshed.
		 * 
		 * */
		WebSocketConnectionManager connectionManager = new WebSocketConnectionManager(new StandardWebSocketClient(),
				new SpringWSHandler(), wsUrl);
		
		/*
		 * Start the execution
		 * 
		 * */
		connectionManager.start();

	}

	/*
	 * Invoked after WebSocket negotiation has succeeded and the WebSocket connection is opened and ready for use.
	 * 
	 * */
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {


		BigDecimal proportionalDelay = null;

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		// Read a wav file from resource folder and convert into BufferedInputStream.
		BufferedInputStream in = new BufferedInputStream(
				new ClassPathResource("1-30.wav").getInputStream());

		int read;
		
		// Define a byte array(1kb)
		byte[] buff = new byte[1024];
		
		//BufferedInputStream values write into ByteArrayOutputStream object.
		while ((read = in.read(buff)) > 0) {
			out.write(buff, 0, read);
		}
		out.flush();
		
		//Convert into byte array.
		byte[] audioBytes = out.toByteArray();

		//Byte array chunk into random size of byte array list.
		List<byte[]> payload = WebsocketUtil.divideRandomArray(audioBytes);

		for (byte[] bs : payload) {

			// Set proportional delay according to 1/16.
			proportionalDelay = new BigDecimal(250);
			proportionalDelay = proportionalDelay.divide(new BigDecimal(4000)).multiply(new BigDecimal(bs.length));

			// Set delay 
			Thread.sleep(proportionalDelay.intValue());
			
			// Send data to the web socket
			session.sendMessage(new BinaryMessage(bs));

		}
		
		// This is a sign of "we are ending our streaming part".Using this flag WebSocket can identify the ending part of the streaming.
		session.sendMessage(new TextMessage("EOS"));
	
	}
	
	/*
	 * Invoked when a new WebSocket message arrives.
	 * In the response payload, till web service sending isLast=TRUE, we are appending partial response messages into one string.
	 * Once it coming as TRUE, we complete the payload string.
	 *
	 * */
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) {
		
		logger.info("message.isLast() >>>" + message.isLast());
		
		if (!message.isLast()) {
            if (this.partialMessageResponseBuilder == null) {
                this.partialMessageResponseBuilder = new StringBuilder();
            }
            this.partialMessageResponseBuilder.append(message.getPayload());
            return;
        }
		
		try {
			
            if(message.isLast() && this.partialMessageResponseBuilder != null) {
            	logger.info("Web Service Response : " + this.partialMessageResponseBuilder.append(message.getPayload()).toString());
            } else {
            	logger.info("Web Service Response : " + message.getPayload());
            }
            
        } catch (Exception e1) {
        	logger.error("Could not parse Web Service Response payload: "+ message.getPayload(), e1);
        }
		
	}

	/*
	 * Handle an error from the underlying WebSocket message transport.
	 * 
	 * */
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) {
		logger.error("Transport Error", exception);
	}

	/*
	 * Invoked after the WebSocket connection has been closed by either side, or after a transport error has occurred. 
	 * Although the session may technically still be open, depending on the underlying implementation, 
	 * sending messages at this point is discouraged and most likely will not succeed.
	 * 
	 * */
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
		logger.info("Connection Closed [" + status.getReason() + "]");
	}

	/*
	 * Whether the WebSocketHandler handles partial messages. 
	 * If this flag is set to true and the underlying WebSocket server supports partial messages, then a large WebSocket message, or 
	 * one of an unknown size may be split and maybe received over multiple calls to handleMessage(WebSocketSession, WebSocketMessage). 
	 * The flag org.springframework.web.socket.WebSocketMessage.isLast() indicates if the message is partial and whether it is the last part.
	 * 
	 * */
	@Override
	public boolean supportsPartialMessages() {
		return true;
	}

}
