package com.cmbk.spring.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cmbk.spring.websocket.handler.SpringWSHandler;

/**
 * @author chanaka.k
 *
 */
@SpringBootApplication
public class SpringWebsocketAppBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebsocketAppBlogApplication.class, args);

		// Start point.
		SpringWSHandler.executeSpringWebSocket();
	}

}
