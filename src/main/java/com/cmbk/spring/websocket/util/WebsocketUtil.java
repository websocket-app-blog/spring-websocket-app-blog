package com.cmbk.spring.websocket.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chanaka.k
 *
 */
public class WebsocketUtil {
	
	private static Logger logger = LoggerFactory.getLogger(WebsocketUtil.class);
	
	/*
	 * Byte array chunk into random size of byte array list.
	 * 
	 * Minimum chunk size is 1kb and maximum size will be 8kb 
	 * 
	 * */
	public static List<byte[]> divideRandomArray(byte[] source) {

		List<byte[]> byteList = new ArrayList<byte[]>();

		int from = 0;
		int to = 0;
		int numberGenerated = 0;
		String log = null;
		
		while (true) {
			
			log = "from: " + from;

			numberGenerated = 1;
			
			// If chunk size is less than 1kb or Odd number, it will re-generate a random number till meet proper value.
			// Minimum chunk size is 1kb and maximum will be 8kb.That is why I took getRandomNumber(1, 9) & getRandomNumber(1, 1025)
			while (numberGenerated < 1025 || numberGenerated % 2 > 0) {
				numberGenerated = getRandomNumber(1, 9) * getRandomNumber(1, 1025);
			}

			logger.info("number Generated: " + numberGenerated);
			
			to = numberGenerated + from;
			
			log += ", to: " + to;
			
			// If source length is grater than 'to' length then we will break the while loop
			if (to > source.length) {
				logger.info("to exceeded: " + to);
				break;
			}
			
			logger.info(log);

			// Adding byte array pieces with given range.
			byteList.add(Arrays.copyOfRange(source, from, to));
			
			logger.info("size: " + (to - from) + "\n\n");
			
			from = to;
		}
		
		logger.info("final from: " + from + ", to: " + source.length);
		
		// Final step : If source length is less than 'to' length, then we will break the while loop
		// and get the difference between 'full source' length and 'current from' length.
		byteList.add(Arrays.copyOfRange(source, from, source.length));
		
		logger.info("size: " + (source.length - from));

		return byteList;
	}
	
	// Generate random numbers with the given number range
	public static int getRandomNumber(int from, int to) {
		return ThreadLocalRandom.current().nextInt(from, to);
	}
}

